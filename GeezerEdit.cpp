/*--------------------------------------------------------------------------*/
/* GeezerEdit.cpp                                                           */
/* Created by Henry Skoglund 2013-09-28  https://tungware.se                */
/*                                                                          */
/* 2013-09-28 First version                                             HS  */
/* 2014-09-14 More ctrl+K chaps                                         HS  */
/* 2017-10-28 Introduce "cursor centering mode" for ctrl-O and ctrl-,   HS  */
/* 2018-12-14 Add ctrl-1 as a shortcut for "//"                         HS  */
/* 2021-08-29 Make cursor more visible (setCursorWidth to 8)            HS  */
/* 2022-03-24 Switch from qmake to CMake (following Qt Creator's lead)  HS  */
/* 2022-08-14 First attempt to support Qt Creator's bookmarks           HS  */
/*--------------------------------------------------------------------------*/

#include <extensionsystem/iplugin.h>

#include <coreplugin/icore.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/idocument.h>
#include <coreplugin/documentmanager.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/editormanager/editormanager.h>
#include <texteditor/texteditor.h>

#include <QApplication>
#include <QAction>
#include <QScrollBar>
#include <QDateTime>

using namespace Core;

// name of our plugin project (need this for the command texts)
static const char* acPluginName = "GeezerEdit";

// we're using these lead-in control chars
static const char cK = 'K';
static const char cQ = 'Q';
static const char c2 = '2';

// setup keyboard modifier value for the Control key (in MacOS Qt parlance: the META key)
#if defined(Q_OS_MACOS)
static const int ctrlModifier = Qt::MetaModifier;
#else
static const int ctrlModifier = Qt::ControlModifier;
#endif

// and for the shift key
static const int shiftModifier = Qt::ShiftModifier;

// max # of milliseconds to go into "cursor centering relative"-mode
static const int nnMaxMSKeepCentering = 1200;

//----------------------------------------------------------------------------
// Declare our plugin class
//----------------------------------------------------------------------------
class GeezerEditPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "GeezerEdit.json")

public:
//-------------------------------------------------------------
// removeKeySequence
//
// 2013-10-02 First version
// 2020-08-27 Support 4.13's multiple key sequences for a cmd
//-------------------------------------------------------------
void removeKeySequence(QKeySequence ks)
{
// see if key sequence already exists in Qt Creator, if so toss it
    for (auto cmd : ActionManager::commands())  // step through all currently in use
    {
    // for this cmd, try to get the first default key sequence
        QKeySequence defaultKeySequence;
        if (cmd->defaultKeySequences().count() > 0)
            defaultKeySequence = cmd->defaultKeySequences().at(0);

    // and the first normal key sequence
        QKeySequence keySequence;
        if (cmd->keySequences().count() > 0)
            keySequence = cmd->keySequences().at(0);

    // do any of match our chap?
         if ((QKeySequence::ExactMatch == ks.matches(defaultKeySequence)) || (QKeySequence::ExactMatch == ks.matches(keySequence)))
            cmd->setKeySequences({});   // yes, so toss this existing cmd (use an empty constructor for the tossing)
    }
}

//-------------------------------------------------------------
// changeKeySequence
//
// 2013-10-09 First version
// 2014-05-09 Also set the default keysequence for the command
//-------------------------------------------------------------
void changeKeySequence(QString sDescription, QKeySequence newKs)
{
// see if a command with this description exists in Qt Creator, if so replace the key sequence
    for (auto cmd : ActionManager::commands())      // step through all currently in use
        if (sDescription == cmd->description())
        {
            cmd->setKeySequences({newKs});
            cmd->setDefaultKeySequence(newKs);
        }
}

//------------------------------------------------------------
// insertKeySequence
//
// 2013-10-02 First version
// 2018-12-16 Switch from QSignalMapper to lambda connects
//------------------------------------------------------------
void insertKeySequence(QKeySequence ks, QByteArray baDescription)
{
// make sure this key sequence is free for us to use by tossing possible previous registered keysequence
    removeKeySequence(ks);

// build a nice command name for this shortcut/key sequence:
// use our plugin name, add a "." and the description (have to strip the spaces though)
    QString sCmdName = tr(acPluginName) + tr(".") + tr(baDescription).remove(tr(" "));

// register this keysequence as a Qt Creator command in the editormanager's context
    auto action  = new QAction(tr(baDescription), this);
    Utils::Id id;
    auto command = ActionManager::registerAction(action,id.withPrefix(qPrintable(sCmdName)),Context(Constants::C_EDITORMANAGER));
    command->setDefaultKeySequence(ks);  // register for both the default and normal key seq.
    command->setKeySequences({ks});

// and wire it up to our receiving function using a lambda
    connect(action,&QAction::triggered,[this,ks]{ gotKeySequence(ks); });
}

//--------------------------------------------------------------------------
// gotKeySequence (called when a registered key seq. is pressed)
//
// 2013-09-28 First version
// 2014-05-08 Add F3 and F4 for open and close comments
// 2015-09-08 Stomp bug: check there is at least one editor window present
// 2017-10-28 Support shrinking/relative ctrl-O ctrl-.
//--------------------------------------------------------------------------
void gotKeySequence(QKeySequence ks)
{
// see have many open editor windows we have right now
    int nWindows = EditorManager::visibleEditors().count();

// if we have at least one window, establish a QPlainTextEdit for the current one
    QPlainTextEdit* pe = nullptr;
    if (nWindows > 0)
        pe = qobject_cast<QPlainTextEdit*>(EditorManager::currentEditor()->widget());

// decode what the current key sequence is
    if ((ks.count() < 1) || (ks.count() > 2))
    // if this key sequence does not consist of 1 or 2 keys then ignore this event
        return;

// decode any prefixes/leadins
    bool bCtrlK = (2 == ks.count()) && ((ctrlModifier + cK) == ks[0]);
    bool bCtrlQ = (2 == ks.count()) && ((ctrlModifier + cQ) == ks[0]);
    bool bCtrl2 = (2 == ks.count()) && ((ctrlModifier + c2) == ks[0]);

// get the final/last key character
    char cKey   = static_cast<char>(ks[static_cast<uint>(ks.count()) - 1]);

// is this a ctrl-K sequence?
    if (bCtrlK)
    {
        switch (cKey)
        {
        case Qt::Key_Q :
        case Qt::Key_X :
        // exit app (by closing all windows)
            qApp->closeAllWindows();
            break;

        case Qt::Key_Y :
        // cut the current selection
            if (pe != nullptr)
                pe->cut();
            break;

        case Qt::Key_S :
        // save all modified files (with no questions asked)
            DocumentManager::saveModifiedDocumentsSilently(DocumentManager::modifiedDocuments());
            break;

        case Qt::Key_C :
        // copy selection (need this one because ctrl-C maps to "goto next page" standard Wordstar cmd)
            if (pe != nullptr)
                pe->copy();
            break;
        }

    // that's all for ctrl-K
        return;
    }

// the rest of this function wants a valid QPlainTextEdit (i.e. at least one editing window opened)
    if (nullptr == pe)
    // no instance found
        return;

// setup a oneliner helper lambda/function (which returns true if text cursor is visible)
    auto isCursorVisible = [pe] { return pe->viewport()->rect().contains(pe->cursorRect()); };

// find out about our current screen real estate
    int nVisibleLines = 1;
    int nCursorLine   = 0;
    if (pe->cursorRect().height() > 0)  // avoid a possible division by zero
    {
    // get (and sanity check) # of visible lines on the editor window
        nVisibleLines = pe->viewport()->height() / pe->cursorRect().height();
        if (nVisibleLines < 1)
            nVisibleLines = 1;

    // get (and sanity check) the line # cursor is on
        nCursorLine   = pe->cursorRect().y()     / pe->cursorRect().height();
        if (nCursorLine >= nVisibleLines)
            nCursorLine = nVisibleLines - 1;
    }

// keep track of # of milliseconds since prev. keypress (used by ctrl-O and ctrl-,)
    static int nPrevMilliSeconds  = 0;
    static int nCursorCenterScale = 0;  // 0 = 1/4 of screen height *regardless* of cur. cursor pos.
                                        // 1 = 1/8 of screen height relative from cur. cursor pos.
                                        // 2 = 1/16 of screen height relative from cur. cursor pos.
// is this a ctrl-Q sequence?
    if (bCtrlQ)
    {
        switch (cKey)
        {
        case Qt::Key_4 :
        // changes cursor width to 4 pixels
            pe->setCursorWidth(4);
            break;

        case Qt::Key_6 :
        // changes cursor width to 6 pixels
            pe->setCursorWidth(6);
            break;

        case Qt::Key_8 :
        // changes cursor width to 8 pixels (done for you automagically when the window opens)
            pe->setCursorWidth(8);
            break;

        case Qt::Key_W :
        // scroll up one page (don't bother moving the cursor)
            pe->verticalScrollBar()->triggerAction(QAbstractSlider::SliderPageStepSub);
            break;

        case Qt::Key_E :
        // goto the top left of the screen
            { // dummy compound statement
                auto cursor = pe->textCursor();
                cursor.movePosition(QTextCursor::Up,QTextCursor::MoveAnchor,nCursorLine);
                cursor.movePosition(QTextCursor::StartOfLine);
                pe->setTextCursor(cursor);
            }
            break;

        case Qt::Key_R :
        // goto the beginning of document
            pe->moveCursor(QTextCursor::Start);
            break;

        case Qt::Key_Y :
        // cut to end of line
            pe->moveCursor(QTextCursor::EndOfLine,QTextCursor::KeepAnchor);
            pe->cut();

        // sometimes cursor is off wandering, correct it
            pe->moveCursor(QTextCursor::EndOfLine);
            break;

        case Qt::Key_S :
        // goto start of line
            pe->moveCursor(QTextCursor::StartOfLine);
            break;

        case Qt::Key_D :
        // goto end of line
            pe->moveCursor(QTextCursor::EndOfLine);
            break;

        case Qt::Key_H :
        // cut to beginning of line
            pe->moveCursor(QTextCursor::StartOfLine,QTextCursor::KeepAnchor);
            pe->cut();
            break;

        case Qt::Key_Z :
        // scroll down one page (don't bother moving the cursor)
            pe->verticalScrollBar()->triggerAction(QAbstractSlider::SliderPageStepAdd);
            break;

        case Qt::Key_X :
        // goto the bottom right of the screen
            { // dummy compound statement
                auto cursor = pe->textCursor();
                cursor.movePosition(QTextCursor::Down,QTextCursor::MoveAnchor,nVisibleLines - (1 + nCursorLine));
                cursor.movePosition(QTextCursor::EndOfLine);
                pe->setTextCursor(cursor);
            }
            break;

        case Qt::Key_C :
        // goto the end of the document
            pe->moveCursor(QTextCursor::End);
            break;

        case Qt::Key_N :
        // switch to next editor window (if we have more than one visible)
            if (nWindows > 1)
            {
                int nCurWindow = EditorManager::visibleEditors().indexOf(EditorManager::currentEditor());
                EditorManager::activateEditor(EditorManager::visibleEditors().at((nCurWindow + 1) % nWindows));
            }
        }

    // that's all for ctrl-Q
        return;
    }

// is this a ctrl-2 prefixed sequence?
    if (bCtrl2)
    {
    // ctrl-2 prefixed commands for inserting text at current cursor position
    // (some of them involves stepping back the cursor)
        switch (cKey)
        {
        case Qt::Key_Q :
            pe->insertPlainText("QVector<>");
            pe->moveCursor(QTextCursor::PreviousCharacter);
            break;

        case Qt::Key_W :
            pe->insertPlainText("TWUtils::");
            break;

        case Qt::Key_E :
            pe->insertPlainText("QDate ");
            break;

        case Qt::Key_R :
            pe->insertPlainText("QStringLiteral(\"\");");
            pe->moveCursor(QTextCursor::PreviousCharacter);
            pe->moveCursor(QTextCursor::PreviousCharacter);
            pe->moveCursor(QTextCursor::PreviousCharacter);
            break;

        case Qt::Key_T :
            pe->insertPlainText("QTime ");
            break;

        case Qt::Key_U :
            pe->insertPlainText("qUtf8Printable();");
            pe->moveCursor(QTextCursor::PreviousCharacter);
            pe->moveCursor(QTextCursor::PreviousCharacter);
            break;

        case Qt::Key_A :
            pe->insertPlainText(".arg()");
            pe->moveCursor(QTextCursor::PreviousCharacter);
            break;

        case Qt::Key_S :
            pe->insertPlainText("QString ");
            break;

        case Qt::Key_D :
            pe->insertPlainText("qDebug() << ");
            break;

        case Qt::Key_F :
            pe->insertPlainText("fubar(\"\");");
            pe->moveCursor(QTextCursor::PreviousCharacter);
            pe->moveCursor(QTextCursor::PreviousCharacter);
            pe->moveCursor(QTextCursor::PreviousCharacter);
            break;

        case Qt::Key_G :
            pe->insertPlainText("QStringList ");
            break;

        case Qt::Key_L :
            pe->insertPlainText("QList<>");
            pe->moveCursor(QTextCursor::PreviousCharacter);
            break;

        case Qt::Key_Z :
            pe->insertPlainText("continue;");
            break;

        case Qt::Key_C :
            pe->insertPlainText(".count()");
            pe->moveCursor(QTextCursor::PreviousCharacter);
            break;

        case Qt::Key_B :
            pe->insertPlainText("QByteArray ");
            break;

        case Qt::Key_N :
            pe->insertPlainText("settings.");
            break;

        case Qt::Key_M :
            pe->insertPlainText("QMap <>");
            pe->moveCursor(QTextCursor::PreviousCharacter);
            break;
        }

    // that's all for the ctrl-2 text insertion commands
        return;
    }

// check for a single legacy Wordstar control key
    switch (cKey)
    {
    case Qt::Key_W :
    // scroll document up one line
        pe->verticalScrollBar()->triggerAction(QAbstractSlider::SliderSingleStepSub);

    // cursor went south? if so, move it back on screen
        if (!isCursorVisible())
            pe->moveCursor(QTextCursor::Up);
        break;

    case Qt::Key_E :
    // move up one line
        pe->moveCursor(QTextCursor::Up);
        break;

    case Qt::Key_R :
    // move up one screenful of lines (i.e. one page)
        { // dummy compound statement
            auto cursor = pe->textCursor();
            cursor.movePosition(QTextCursor::Up,QTextCursor::MoveAnchor,nVisibleLines);
            pe->setTextCursor(cursor);
        }
        break;

    case Qt::Key_T :
    // cut next word
        pe->moveCursor(QTextCursor::WordRight,QTextCursor::KeepAnchor);
        pe->cut();
        break;

    case Qt::Key_Y :
    // cut line under cursor including EOL
        pe->moveCursor(QTextCursor::StartOfLine);
        pe->moveCursor(QTextCursor::EndOfLine    ,QTextCursor::KeepAnchor);
        pe->moveCursor(QTextCursor::NextCharacter,QTextCursor::KeepAnchor);
        pe->cut();
        break;

    case Qt::Key_U :
    // undo
        pe->undo();
        break;

    case Qt::Key_O :
    // center cursor in higher 1/4 part (absolute or relative to current cursor pos.)
        if (nVisibleLines > 2)
        {
        // get cur. # of milliseconds since midnight
            int nCurMilliSeconds = QTime::currentTime().msecsSinceStartOfDay();

        // is this within ms limits of a previous ctrl-O or ctrl-,/ctrl-.?
            if ((nCurMilliSeconds - nPrevMilliSeconds) < nnMaxMSKeepCentering)
                ++nCursorCenterScale;    // yes step it up
            else
                nCursorCenterScale = 0;  // no, back to power-on default

        // remember the time for this ctrl-O
            nPrevMilliSeconds = nCurMilliSeconds;

        // absolute or relative screen walking?
            if (0 == nCursorCenterScale)
            {
            // absolute (default), start on the top line and step down 1/4 of the visible lines
                auto tc = pe->cursorForPosition(pe->viewport()->pos());
                tc.movePosition(QTextCursor::Down,QTextCursor::MoveAnchor,((nVisibleLines + 1) / 4) - 1);

            // try to center horizontally as well (if line has 2 chars or more)
                tc.movePosition(QTextCursor::EndOfLine);
                if (tc.columnNumber() > 1)
                    tc.movePosition(QTextCursor::PreviousCharacter,QTextCursor::MoveAnchor,tc.columnNumber() / 2);

                pe->setTextCursor(tc);
            }

        // relative?
            if (nCursorCenterScale > 0)
            {
            // calc # of lines to move up
                int nLinesUp = (nVisibleLines + 1) / (8 * nCursorCenterScale);

                if (nLinesUp < 1)
                    nLinesUp = 1;

                for (int l = 0; (l < nLinesUp); ++l)
                    pe->moveCursor(QTextCursor::Up);
            }
        }
        break;

    case Qt::Key_A :
    // previous word
        pe->moveCursor(QTextCursor::PreviousWord);
        break;

    case Qt::Key_S :
    // previous character
        pe->moveCursor(QTextCursor::PreviousCharacter);
        break;

    case Qt::Key_D :
    // next character
        pe->moveCursor(QTextCursor::NextCharacter);
        break;

    case Qt::Key_F :
    // next word
        pe->moveCursor(QTextCursor::NextWord);
        break;

    case Qt::Key_G :
    // cut next char
        pe->moveCursor(QTextCursor::NextCharacter,QTextCursor::KeepAnchor);
        pe->cut();
        break;

    case Qt::Key_H :
    // cut prev. char
        pe->moveCursor(QTextCursor::PreviousCharacter,QTextCursor::KeepAnchor);
        pe->cut();
        break;

    case Qt::Key_L :
    // center cursor in visible area
        nCursorCenterScale = 0;     // reset the scale, this one is always absolute

        if (nVisibleLines > 2)
        {
        // start on the top line and move down 1/2 of the visible lines
            auto tc = pe->cursorForPosition(pe->viewport()->pos());
            tc.movePosition(QTextCursor::Down,QTextCursor::MoveAnchor,(nVisibleLines / 2) - 1);

        // center horizontally as well (if line has 2 chars or more)
            tc.movePosition(QTextCursor::EndOfLine);
            if (tc.columnNumber() > 1)
                tc.movePosition(QTextCursor::PreviousCharacter,QTextCursor::MoveAnchor,tc.columnNumber() / 2);

            pe->setTextCursor(tc);
        }
        break;

    case Qt::Key_Z :
    // scroll document down one line
        pe->verticalScrollBar()->triggerAction(QAbstractSlider::SliderSingleStepAdd);

    // cursor went north? if so, move it back on screen
        if (!isCursorVisible())
            pe->moveCursor(QTextCursor::Down);
        break;

    case Qt::Key_X :
    // move down one line
        pe->moveCursor(QTextCursor::Down);
        break;

    case Qt::Key_C :
    // move down one screenful of lines (i.e. one page)
        { // dummy compound statement
            auto cursor = pe->textCursor();
            cursor.movePosition(QTextCursor::Down,QTextCursor::MoveAnchor,nVisibleLines);
            pe->setTextCursor(cursor);
        }
        break;

    case Qt::Key_V :
    // paste (do this on the Mac, nicely complements the normal Command-V)
        pe->paste();
        break;

    case Qt::Key_N :
    // insert class name at cursor (note: assumes classname == filename without extension)
        { // dummy compound statement
        // get the name of the document for the current window
            QString sFilename = EditorManager::currentDocument()->displayName();

        // toss possible trailing ".h" and ".cpp" extensions
            sFilename.remove(tr(".h")  ,Qt::CaseInsensitive);
            sFilename.remove(tr(".cpp"),Qt::CaseInsensitive);

        // reformat it into a nice classname; first, make sure 1st letter is uppercase
            QString sClassname = sFilename.left(1).toUpper() + sFilename.mid(1);

        // do some further camelcase tweaks for some popular Qt classnames
            if (sClassname == tr("Mainwindow"))
                sClassname = tr("MainWindow");

        // done, insert the string at cursor position
            pe->insertPlainText(sClassname);
        }
        break;

    case Qt::Key_Comma :
    case Qt::Key_Period :
    // center cursor in lower 1/4 part (absolute or relative to current cursor pos.)
        if (nVisibleLines > 2)
        {
        // get cur. # of milliseconds since midnight
            int nCurMilliSeconds = QTime::currentTime().msecsSinceStartOfDay();

        // is this within ms limits of a previous ctrl-O or ctrl-,?
            if ((nCurMilliSeconds - nPrevMilliSeconds) < nnMaxMSKeepCentering)
                ++nCursorCenterScale;    // yes step it up
            else
                nCursorCenterScale = 0;  // no, back to power-on default

        // remember the time for this ctrl-, or ctrl-.
            nPrevMilliSeconds = nCurMilliSeconds;

        // absolute or relative screen walking?
            if (0 == nCursorCenterScale)
            {
            // absolute, start on the top line and step down 3/4 of the visible lines
                auto tc = pe->cursorForPosition(pe->viewport()->pos());
                tc.movePosition(QTextCursor::Down,QTextCursor::MoveAnchor,((3 * nVisibleLines + 1) / 4) - 1);

            // center horizontally as well (if line has 2 chars or more)
                tc.movePosition(QTextCursor::EndOfLine);
                if (tc.columnNumber() > 1)
                    tc.movePosition(QTextCursor::PreviousCharacter,QTextCursor::MoveAnchor,tc.columnNumber() / 2);

                pe->setTextCursor(tc);
            }

        // relative?
            if (nCursorCenterScale > 0)
            {
            // calc # of lines to move down
                int nLinesDown = (nVisibleLines + 1) / (8 * nCursorCenterScale);

                if (nLinesDown < 1)
                    nLinesDown = 1;

                for (int l = 0; (l < nLinesDown); ++l)
                    pe->moveCursor(QTextCursor::Down);
            }
        }
        break;

    // that's all for single control keys
    }

// also check for some custom function keys
    QString sHyphen       = tr("-");
    QString sSpace        = tr(" ");
    QString sEOL          = tr("\n");
    QString sAuthor       = tr("HS ");  // pls replace with your own initials

    QString sFilename     = EditorManager::currentDocument()->displayName();
    bool    bCPPOrHFile   = ((0 == sFilename.right(4).compare(tr(".cpp"),Qt::CaseInsensitive)) ||
                             (0 == sFilename.right(2).compare(tr(".h")  ,Qt::CaseInsensitive)));

    QString sCommentBegin = bCPPOrHFile ? tr("/*") : tr("#");
    QString sCommentEnd   = bCPPOrHFile ? tr("*/") : tr("#");
    QString sComment      = bCPPOrHFile ? tr("//") : tr("#");

// is it ctrl-1 (insert comment)?
    if (QKeySequence(ctrlModifier + Qt::Key_1) == ks)
        pe->insertPlainText(sComment);

// is it F3 (insert begin of comment)?
    if (QKeySequence(Qt::Key_F3) == ks)
        pe->insertPlainText(sCommentBegin);

// is it F4 (insert end of comment)?
    if (QKeySequence(Qt::Key_F4) == ks)
        pe->insertPlainText(sCommentEnd);

// shift-F3 insert file header at beginning of file?
    if (QKeySequence(shiftModifier + Qt::Key_F3) == ks)
    {
        const int nLineSize  = 78;
        int nCount           = nLineSize - QString(sCommentBegin + sCommentEnd).length();
        QString sHyphenLine  = sCommentBegin + sHyphen.repeated(nCount) + sCommentEnd;

    // get the name of the document for the current window
        QString sName        = EditorManager::currentDocument()->displayName();

    // create today's date etc.
        int nNameCount       = nLineSize - QString(sCommentBegin + sSpace + sName + sCommentEnd).length();
        QString sNameLine    = sCommentBegin + sSpace + sName + sSpace.repeated(nNameCount) + sCommentEnd;
        QString sEmptyLine   = sCommentBegin + sSpace.repeated(nCount) + sCommentEnd;

        QString sToday       = QDate::currentDate().toString(tr("yyyy-MM-dd"));
        QString sVersion     = tr("First version");
        nCount               = nLineSize - QString(sCommentBegin + sSpace + sToday + sSpace + sVersion + sAuthor + sSpace + sCommentEnd).length();
        QString sVersionLine = sCommentBegin + sSpace + sToday + sSpace + sVersion + sSpace.repeated(nCount) + sAuthor + sSpace + sCommentEnd;

    // insert all in one fell swoop (for a nice undo/CTRL+U experience)
    // (note: use a cursor for textinsertion so we're not messing with the user's position)
        QTextCursor cursor = pe->textCursor();
        cursor.movePosition(QTextCursor::Start);
        cursor.insertText(sHyphenLine + sEOL + sNameLine + sEOL + sEmptyLine + sEOL + sVersionLine + sEOL + sHyphenLine + sEOL + sEOL);
    }

// shift-F5 insert method header?
    if (QKeySequence(shiftModifier + Qt::Key_F5) == ks)
    {
        const int nLineSize  = 78;
        int nCount           = nLineSize - sComment.length();
        QString sHyphenLine  = sComment + sHyphen.repeated(nCount);
        QString sEmptyLine   = sComment;

        auto cursor          = pe->textCursor();
        cursor.movePosition(QTextCursor::StartOfLine);
        cursor.movePosition(QTextCursor::EndOfLine  ,QTextCursor::KeepAnchor);
        QString sName = cursor.selectedText();

    // try to extract the method name (only support C and C++, not Objective-C)
        sName = sName.left(sName.indexOf(tr("(")));          // toss leftmost parenthesis and chars thereafter
        sName = sName.mid(1 + sName.lastIndexOf(tr(" ")));   // toss upto and including rightmost space
        if (sName.indexOf(tr("::")) > 0)
        // C++ method name?
            sName = sName.mid(2 + sName.lastIndexOf(tr("::")));  // toss upto and including rightmost "::"

        QString sNameLine    = sComment + sSpace + sName;

    // got method name (presumably :-); create today's date
        QString sToday       = QDate::currentDate().toString(tr("yyyy-MM-dd"));
        QString sVersion     = tr("First version");
        QString sVersionLine = sComment + sSpace + sToday + sSpace + sVersion;

    // insert all in one fell swoop (for a nice undo/CTRL-U experience)
    // (note: use a cursor for textinsertion so we're not messing with the user's position)
        cursor.movePosition(QTextCursor::StartOfLine);
        cursor.insertText(sHyphenLine + sEOL + sNameLine + sEOL + sEmptyLine + sEOL + sVersionLine + sEOL + sHyphenLine + sEOL);
    }

// is it F6 (insert today's date)?
    if (QKeySequence(Qt::Key_F6) == ks)
        pe->insertPlainText(QDate::currentDate().toString(tr("yyyy-MM-dd")));

// is it shift F6 = insert '#include '?
    if (QKeySequence(shiftModifier + Qt::Key_F6) == ks)
        pe->insertPlainText(tr("#include "));

// end of gotKeySequence
}

// use one of the following methods to register a key sequence into Qt Creator
//-------------------------------------------------------------------------------
// addChar (5 flavors)
//
// 2013-10-02 First version
// 2013-10-05 Allocate twin orbs for the ctrl-K and ctrl-Q chaps
// 2013-10-06 Fix duplicate descriptions bug/feature by appending " #1" or " #2"
// 2014-05-08 Support adding other keys than control keys, e.g. function keys
// 2016-06-05 Introduce ctrl-Q ctrl-Q leadin for more alphabetic insertion chaps
// 2018-12-16 Toss ctrl-Q ctrl-Q prefix and switch to ctrl-2 prefix
//-------------------------------------------------------------------------------
void addCtrlKAndChar(char cKey, QByteArray baDescription)
{
// use this method flavor for ctrl-K prefixed commands
// insert 2 key sequences: one with ctrl-K and ctrl-key, one with ctrl-K and key (without ctrl)
// append " #1" or " #2" to disambiguate the descriptions (Qt Creator *really* hates duplicates)
    insertKeySequence(QKeySequence(ctrlModifier + cK,ctrlModifier + cKey),baDescription + " #1");
    insertKeySequence(QKeySequence(ctrlModifier + cK,               cKey),baDescription + " #2");
}

void addCtrlQAndChar(char cKey, QByteArray baDescription)
{
// use this method for ctrl-Q prefixed commands
// insert 2 key sequences, one with ctrl-Q and ctrl-key and one with ctrl-Q and key (without ctrl)
// append " #1" or " #2" to disambiguate the descriptions (did I mention that Qt Creator hates duplicates?)
    insertKeySequence(QKeySequence(ctrlModifier + cQ,ctrlModifier + cKey),baDescription + " #1");
    insertKeySequence(QKeySequence(ctrlModifier + cQ,               cKey),baDescription + " #2");
}

void addCtrl2AndChar(char cKey, QByteArray baDescription)
{
// use this method for ctrl-2 prefixed commands
// insert 2 key sequences, one with ctrl-2 and ctrl-key and one with ctrl-2 and key (without ctrl)
// append a " #1" or " #2" to disambiguate the descriptions
    insertKeySequence(QKeySequence(ctrlModifier + c2,ctrlModifier + cKey),baDescription + " #1");
    insertKeySequence(QKeySequence(ctrlModifier + c2,               cKey),baDescription + " #2");
}

void addCtrlChar(char cKey, QByteArray baDescription)
{
// insert one control character (vanilla Wordstar command)
    insertKeySequence(QKeySequence(ctrlModifier + cKey),baDescription);
}

void addAnyKey(int iKey, QByteArray baDescription)
{
// insert any character as a command (for example a function key)
    insertKeySequence(QKeySequence(iKey),baDescription);
}

//-------------------------------------------------------------------------
// delayedInitialize
// (use delayed init() because we're playing a game of chicken backwards)
//
// 2013-09-28 First version
// 2013-10-09 Support search/replace by changing ctrl-F to ctrl-Q ctrl-F
//-------------------------------------------------------------------------
bool delayedInitialize()
{
// make sure ctrl-K, ctrl-Q and ctrl-2 leadin chars are unused/free for us
// by removing all existing ctrl-K, ctrl-Q and ctrl-2 commands
    removeKeySequence(ctrlModifier + cK);
    removeKeySequence(ctrlModifier + cQ);
    removeKeySequence(ctrlModifier + c2);

// add ctrl-K prefixed commands
    addCtrlKAndChar('Q',"Quit");
    addCtrlKAndChar('Y',"Cut Selection");
    addCtrlKAndChar('S',"Save All");
    addCtrlKAndChar('X',"Exit");
    addCtrlKAndChar('C',"Copy Selection");

// add ctrl-Q prefixed commands
    addCtrlQAndChar('4',"Sets cursor width to 4 pixels.");
    addCtrlQAndChar('6',"Sest cursor width to 6 pixels.");
    addCtrlQAndChar('8',"Sets cursor width to 8 pixels.");
    addCtrlQAndChar('W',"Scroll Up One Page");
    addCtrlQAndChar('E',"Go to Top Left of Screen");
    addCtrlQAndChar('R',"Go to Beginning of Document");
    addCtrlQAndChar('Y',"Cut to End of Line");
    addCtrlQAndChar('S',"Go to Start of Line");
    addCtrlQAndChar('D',"Go to End of Line");
    addCtrlQAndChar('H',"Cut to Beginning of Line");
    addCtrlQAndChar('Z',"Scroll Down One Page");
    addCtrlQAndChar('X',"Go to Bottom Right of Screen");
    addCtrlQAndChar('C',"Go to End of Document");
    addCtrlQAndChar('N',"Go to Next Window");

// add ctrl-2 prefixed text insertion commands
    addCtrl2AndChar('Q',"Insert 'QVector<>'.");
    addCtrl2AndChar('W',"Insert 'TWUtils::'.");
    addCtrl2AndChar('E',"Insert 'QDate '.");
    addCtrl2AndChar('R',"Insert 'QStringLiteral(\"\");'.");
    addCtrl2AndChar('T',"Insert 'QTime '.");
    addCtrl2AndChar('U',"Insert 'qUtf8Printable(\"\");'.");
    addCtrl2AndChar('A',"Insert '.arg()'.");
    addCtrl2AndChar('S',"Insert 'QString '.");
    addCtrl2AndChar('D',"Insert 'qDebug() << '.");
    addCtrl2AndChar('F',"Insert 'fubar(\"\");'.");
    addCtrl2AndChar('G',"Insert 'QStringList '.");
    addCtrl2AndChar('L',"Insert 'QList<>'.");
    addCtrl2AndChar('Z',"Insert 'continue;'.");
    addCtrl2AndChar('C',"Insert '.count()'.");
    addCtrl2AndChar('B',"Insert 'QByteArray '.");
    addCtrl2AndChar('N',"Insert 'settings.'.");
    addCtrl2AndChar('M',"Insert 'QMap<>'.");

// finally the single, stand-alone ctrl-keys
    addCtrlChar('W',"Scroll Up Line");
    addCtrlChar('E',"Go to Previous Line");
    addCtrlChar('R',"Go to Previous Page");
    addCtrlChar('T',"Cut Next Word");
    addCtrlChar('Y',"Cut Line");
    addCtrlChar('U',"Undo");
    addCtrlChar('O',"Go to Upper part of Screen");

    addCtrlChar('A',"Go to Previous Word");
    addCtrlChar('S',"Go to Previous Character");
    addCtrlChar('D',"Go to Next Character");
    addCtrlChar('F',"Go to Next Word");
    addCtrlChar('G',"Cut Next Character");
    addCtrlChar('H',"Cut Previous Character");
    addCtrlChar('L',"Go to Center of Screen");

    addCtrlChar('Z',"Scroll Down Line");
    addCtrlChar('X',"Go to Next Line");
    addCtrlChar('C',"Go to Next Page");
    addCtrlChar('N',"Insert ''filename''' at cursor pos.");
    addCtrlChar(',',"Go to Lower part of Screen#1");   // support both comma and period (for convenience)
    addCtrlChar('.',"Go to Lower part of Screen#2");
#if defined(Q_OS_MACOS)
    addCtrlChar('V',"Paste");  // ctrl-V is used on Windows/Linux, but support it on MacOS as well
#endif

// setup the find and replace commands, by replacing their keyboard shortcuts
// reroute "Find/Replace" to ctrl-Q ctrl-F and "Replace" to ctrl-Q ctrl-A
// (note: currently these two have no "#2" command twin, i.e. typing ctrl-Q F is not supported)
    changeKeySequence(tr("Find/Replace")        ,QKeySequence(ctrlModifier + cQ,ctrlModifier + Qt::Key_F));
    changeKeySequence(tr("Replace")             ,QKeySequence(ctrlModifier + cQ,ctrlModifier + Qt::Key_A));

// add ctrl-K chaps for toggling mode selector and left sidebar visibility
    changeKeySequence(tr("Show Mode Selector")  ,QKeySequence(ctrlModifier + cK,Qt::Key_1));
    changeKeySequence(tr("Show Left Sidebar")   ,QKeySequence(ctrlModifier + cK,Qt::Key_2));

// tweak the split window commands to Wordstar flavoring (ctrl-K nn)
    changeKeySequence(tr("Split Side by Side")  ,QKeySequence(ctrlModifier + cK,Qt::Key_3));
    changeKeySequence(tr("Split")               ,QKeySequence(ctrlModifier + cK,Qt::Key_4));
    changeKeySequence(tr("Remove All Splits")   ,QKeySequence(ctrlModifier + cK,Qt::Key_9));
    changeKeySequence(tr("Remove Current Split"),QKeySequence(ctrlModifier + cK,Qt::Key_0));

// use ctrl-K,N to switch header/source in current window (instead of F4) (note ctrl-K, ctrl-N will not work :-(
    changeKeySequence(tr("Switch Header/Source"),QKeySequence(ctrlModifier + cK,Qt::Key_N));

// assign ctrl-1 for "//" + F3 for begin comment and F4 for end comment
    addAnyKey(ctrlModifier + Qt::Key_1,"Insert //");
    addAnyKey(Qt::Key_F3,"Begin comment");
    addAnyKey(Qt::Key_F4,"End comment"  );

// assign F5 for Run, F6 to insert today's date and F7 for Build All
    auto F5ForRun = QKeySequence(Qt::Key_F5);
    removeKeySequence(F5ForRun);                 // toss all prev. (F5 is used plenty in vanilla Qt Creator)
    changeKeySequence(tr("Run"),F5ForRun);
    addAnyKey(Qt::Key_F6,"Insert today's date"); // needs some code to function (see above)
    changeKeySequence(tr("Build All"),QKeySequence(Qt::Key_F7));

// use shift-F3 to insert a nice file header, shift-F5 for a method header and shift-F6 to insert "#include "
    addAnyKey(shiftModifier + Qt::Key_F3,"Insert file header");
    addAnyKey(shiftModifier + Qt::Key_F5,"Insert method header");
    addAnyKey(shiftModifier + Qt::Key_F6,"Insert '#include '");

// add ctrl-Q K to go to the next bookmark (if any)
    changeKeySequence(tr("Next Bookmark"),QKeySequence(ctrlModifier + cQ,ctrlModifier + Qt::Key_K));

// add ctrl-K ctrl-P as a shortcut to Preferences
    changeKeySequence(tr("Preferences..."),QKeySequence(ctrlModifier + cK,ctrlModifier + Qt::Key_P));

// that's all, we're done initializing
    return true;
}

//----------------------------------------------------------------------------
// initialize
//
// 2013-09-28 First version
//----------------------------------------------------------------------------
bool initialize(const QStringList& arguments, QString* errorString)
{
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);
    return true;
}

//----------------------------------------------------------------------------
// extensionsInitialized
//
// 2013-09-28 First version
// 2021-08-29 Wire up EditorManager::editorOpened to change the cursor width
//----------------------------------------------------------------------------
void extensionsInitialized()
{
// most of Qt Creator is up and running at this point
// so now is a good time to connect to the EditorManager::editorOpened signal
    connect(EditorManager::instance(),&EditorManager::editorOpened,this,[] (IEditor* editor)
    {
    // 2021-08-29: try to fiddle with the cursor height here (set it to 8)
        if (editor == nullptr)
        // no editor no dice
            return;

        if (editor->widget() == nullptr)
        // no widget no problem
            return;

        auto widget = editor->widget();
        auto pe     = qobject_cast<QPlainTextEdit*>(widget);
        if (pe != nullptr)
            pe->setCursorWidth(8);
    } );
}

//----------------------------------------------------------------------------
// aboutToShutdown
//
// 2013-09-28 First version
//----------------------------------------------------------------------------
ShutdownFlag aboutToShutdown()
{
    return SynchronousShutdown;
}

// end of class GeezerEditPlugin
};

// 2022-03-24 Skip the .h file and solve the mocing by #including it here
#include "GeezerEdit.moc"
